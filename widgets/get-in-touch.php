<?php
add_action('widgets_init', 'our_hours_load_widgets');

function our_hours_load_widgets()
{
	register_widget('Our_Hours_Widget');
}

class Our_Hours_Widget extends WP_Widget {
	
	function Our_Hours_Widget()
	{
		$widget_ops = array('classname' => 'hours', 'description' => 'Full Width Footer Widget');

		$control_ops = array('id_base' => 'our-hours-widget');

		$this->WP_Widget('our-hours-widget', 'Progression: Need Assitance?', $widget_ops, $control_ops);
	}
	
	function widget($args, $instance)
	{
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		$text_contact_1 = $instance['text_contact_1'];
		$text_contact_2 = $instance['text_contact_2'];


		echo $before_widget;
	 ?>
		
		<!-- start widget -->
		<div id="featured-assistance-pro">
			<div class="width-container-pro">
				<?php if($title): ?><h3><?php echo $title; ?></h3><?php endif; ?>
				<?php if($text_contact_1): ?><h4><?php echo $text_contact_1; ?></h4><?php endif; ?>
					
				
				<?php if($text_contact_2): ?><p><?php echo $text_contact_2; ?></p><?php endif; ?>
				<div class="clearfix"></div>
			</div><!-- close .width-container -->
		</div><!-- close #contact-base-avanter -->
		<!-- end widget -->
	

		<?php echo $after_widget;
	}
	
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['text_contact_1'] = $new_instance['text_contact_1'];
		$instance['text_contact_2'] = $new_instance['text_contact_2'];
		
		return $instance;
	}

	function form($instance)
	{
		$defaults = array('title' => 'Need Assistance?', 'text_contact_1' => 'By Phone: 1-888-777-1546', 'text_contact_2' => 'Sample text here...');
		$instance = wp_parse_args((array) $instance, $defaults); ?>
		
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id('text_contact_1'); ?>">Sub-Heading:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('text_contact_1'); ?>" name="<?php echo $this->get_field_name('text_contact_1'); ?>" value="<?php echo $instance['text_contact_1']; ?>" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id('text_contact_2'); ?>">Additional Text:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('text_contact_2'); ?>" name="<?php echo $this->get_field_name('text_contact_2'); ?>" value="<?php echo $instance['text_contact_2']; ?>" />
		</p>

		
	<?php
	}
}
?>