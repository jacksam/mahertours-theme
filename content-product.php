<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = get_theme_mod('shop_col_progression', '3');

// Ensure visibility
if ( ! $product || ! $product->is_visible() )
	return;

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] )
	$classes[] = 'first';
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] )
	$classes[] = 'last';
?>
<li class="product column-<?php echo get_theme_mod('shop_col_progression', '3'); ?> <?php
	if ( $woocommerce_loop['loop'] % $woocommerce_loop['columns'] == 0 )
		echo 'last';
	elseif ( ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] == 0 )
		echo 'first';
	?>">
	
	<div class="product-container-pro">
	
	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>

	<a href="<?php the_permalink(); ?>">

		<div class="product-image-pro">
			<?php
				/**
				 * woocommerce_before_shop_loop_item_title hook
				 *
				 * @hooked woocommerce_show_product_loop_sale_flash - 10
				 * @hooked woocommerce_template_loop_product_thumbnail - 10
				 */
				do_action( 'woocommerce_before_shop_loop_item_title' );
			?>
		</div>
		</a>
		
		<?php
		            $size = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
		            echo $product->get_categories( ', ', '<div class="category-location-pro"><i class="fa fa-map-marker"></i>' . _n( '', '', $size, 'progression' ) . ' ', '</div>' );
		    ?>
		
		<a href="<?php the_permalink(); ?>">
		<h3><?php the_title(); ?></h3>

		<?php
			/**
			 * woocommerce_after_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_template_loop_rating - 5
			 * @hooked woocommerce_template_loop_price - 10
			 */
			// do_action( 'woocommerce_after_shop_loop_item_title' );
		?>
	</a>
  
	<?php
    $departs = get_the_terms( $product->id, 'pa_tour-departs' );
    if ( $departs && ! is_wp_error( $departs ) ) : 
      foreach ( $departs as $depart ) {
        echo '<div class="tour-departs"><span>Tour departs:</span> '.$depart->name.'</div>';
    }
    endif;
  ?>
  
  <?php
    $lengths = get_the_terms( $product->id, 'pa_tour-length' );
    if ( $lengths && ! is_wp_error( $lengths ) ) : 
      foreach ( $lengths as $length ) {
        echo '<div class="tour-length"><span>Tour length:</span> '.$length->name.'</div>';
    }
    endif;
  ?>

	<?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
	
	</div><!-- close .product-container-pro -->
</li>