<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $post, $product;
?>
<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">

	<p class="price"><?php /*?><?php echo $product->get_price_html(); ?><?php */?></p>
  
  <?php
    $departs = get_the_terms( $product->id, 'pa_tour-departs' );
    if ( $departs && ! is_wp_error( $departs ) ) : 
      foreach ( $departs as $depart ) {
        echo '<div class="tour-departs"><span>Tour departs:</span> '.$depart->name.'</div>';
    	}
    endif;
  ?>
  
  <?php
    $lengths = get_the_terms( $product->id, 'pa_tour-length' );
    if ( $lengths && ! is_wp_error( $lengths ) ) : 
      foreach ( $lengths as $length ) {
        echo '<div class="tour-length"><span>Tour length:</span> '.$length->name.'</div>';
    }
    endif;
  ?>

	<meta itemprop="price" content="<?php echo $product->get_price(); ?>" />
	<meta itemprop="priceCurrency" content="<?php echo get_woocommerce_currency(); ?>" />
	<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />

</div>