/*  Table of Contents 
01. MENU ACTIVATION
02. GALLERY JAVASCRIPT
03. FITVIDES RESPONSIVE VIDEOS
04. MOBILE SELECT MENU
05. CHOSEN
06. RESPONSIVE TABS
*/
/*
=============================================== 01. MENU ACTIVATION  ===============================================
*/
jQuery(document).ready(function($) {
	jQuery("ul.sf-menu").supersubs({ 
	        minWidth:    5,   // minimum width of sub-menus in em units 
	        maxWidth:    25,   // maximum width of sub-menus in em units 
	        extraWidth:  1     // extra width can ensure lines don't sometimes turn over 
	                           // due to slight rounding differences and font-family 
	    }).superfish({ 
			animationOut:  {opacity:'hide'},
			speed:         300,           // speed of the opening animation. Equivalent to second parameter of jQuery’s .animate() method
			speedOut:      'normal',
			autoArrows:    true,               // if true, arrow mark-up generated automatically = cleaner source code at expense of initialisation performance 
			dropShadows:   false,               // completely disable drop shadows by setting this to false 
			delay:     400               // 1.2 second delay on mouseout 
		});
});


/*
=============================================== 02. GALLERY JAVASCRIPT  ===============================================
*/
jQuery(document).ready(function($) {
    $('.gallery-progression').flexslider({
		animation: "fade",      
		slideDirection: "horizontal", 
		slideshow: false,         
		slideshowSpeed: 7000,  
		animationDuration: 200,        
		directionNav: true,             
		controlNav: true               
    });
});

jQuery(document).ready(function($) {
	$("a[rel^='prettyPhoto[product-gallery]']").prettyPhoto();
});


/*
=============================================== 03. FITVIDES RESPONSIVE VIDEOS  ===============================================
*/
jQuery(document).ready(function($) {  
$("#main").fitVids();
$(".flexslider").fitVids();
});


/*
=============================================== 04. MOBILE SELECT MENU  ===============================================
*/
jQuery(document).ready(function($) {
$('.sf-menu').mobileMenu({
    defaultText: 'Navigate to...',
    className: 'select-menu',
    subMenuDash: '&ndash;&ndash;'
});

$('.filter-children').mobileMenu({
    defaultText: 'Navigate to...',
    className: 'select-menu',
    subMenuDash: '&ndash;&ndash;'
});
});


/*
=============================================== 05. CHOSEN  ===============================================
*/
jQuery(document).ready(function($) {
	$('#brochure-tours').attr("data-placeholder", "Select tours...");
	$('.wpcf7-form select, .orderby').chosen({
		width: "100%",
		disable_search_threshold: 10
	});
});


/*
=============================================== 06. RESPONSIVE TABS  ===============================================
*/
jQuery(document).ready(function($) {
	if($('.woocommerce-tabs').length > 0) {
		$('.woocommerce-tabs').responsiveTabs({
			startCollapsed: 'accordion',
			animation: 'slide'
		});
	}
});
