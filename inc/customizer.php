<?php
/**
 * progression Theme Customizer
 *
 * @package progression
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function progression_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', 'progression_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function progression_customize_preview_js() {
	wp_enqueue_script( 'progression_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'progression_customize_preview_js' );



/**
 * Adds the individual sections, settings, and controls to the theme customizer
 */
function progression_customizer( $wp_customize ) {
	
	// Adds abaillity to add text area
	if ( class_exists( 'WP_Customize_Control' ) ) { 
		# Adds textarea support to the theme customizer 
		class ProgressionTextAreaControl extends WP_Customize_Control { 
			public $type = 'textarea'; 
			public function __construct( $manager, $id, $args = array() ) { 
				$this->statuses = array( '' => __( 'Default', 'progression' ) ); 
				parent::__construct( $manager, $id, $args ); 
			}   
			
			public function render_content() { 
				echo '<label> 
				<span class="customize-control-title">' . esc_html( $this->label ) . '</span> 
				<textarea rows="5" style="width:100%;" '; $this->link(); echo '>' . esc_textarea( $this->value() ) . '</textarea> 
				</label>'; } 
			}   
		}
		
//Add Section Page of Theme Settings
    $wp_customize->add_section(
        'options_panel_progression',
        array(
            'title' => __('Theme Settings', 'progression'),
            'description' => __('Main Theme Settings', 'progression'),
            'priority' => 35,
        )
    );
	
	
	// Add Copyright Text
	$wp_customize->add_setting(
	    'header_number_pro',
	    array(
	        'default' => 'Main office: (888)-777-1546',
	    )
	);

	$wp_customize->add_control(new ProgressionTextAreaControl( $wp_customize, 'header_number_pro', array( 
		'label' => __('Header Top Left Text', 'progression'), 
		'section' => 'options_panel_progression', 
		'settings' => 'header_number_pro', 
		'priority' => 2, ) 
		)
	);
	
	
	// Add Copyright Text
	$wp_customize->add_setting(
	    'header_subscribe_pro',
	    array(
	        'default' => '<a href="#"><i class="fa fa-envelope-o"></i> Subscribe to our newsletter</a>',
	    )
	);

	$wp_customize->add_control(new ProgressionTextAreaControl( $wp_customize, 'header_subscribe_pro', array( 
		'label' => __('Header Top Right Text', 'progression'), 
		'section' => 'options_panel_progression', 
		'settings' => 'header_subscribe_pro', 
		'priority' => 3, ) 
		)
	);
	
	
	//Logo Uploader
	$wp_customize->add_setting( 
		'logo_upload' ,
		array(
	        'default' => get_template_directory_uri().'/images/logo.png',
	    )
	);
	$wp_customize->add_control(
	    new WP_Customize_Image_Control(
	        $wp_customize,
	        'logo_upload',
	        array(
	            'label' => __('Theme Logo', 'progression'), 
	            'section' => 'options_panel_progression',
	            'settings' => 'logo_upload',
					'priority'   => 5,
	        )
	    )
	);
	
	//Logo Width
	$wp_customize->add_setting( 
		'logo_width' ,
		array(
	        'default' => '165',
	    )
	);
	$wp_customize->add_control(
   'logo_width',
   	array(
	   	'label' => __('Logo Width', 'progression'), 
			'section' => 'options_panel_progression',
			'type' => 'text',
			'priority'   => 7,
	    )
	);
	
	//Header Padding
	$wp_customize->add_setting( 
		'header_padding' ,
		array(
	        'default' => '36',
	    )
	);
	$wp_customize->add_control(
   'header_padding',
   	array(
	   	'label' => __('Navigation Margin Top/Bottom', 'progression'), 
			'section' => 'options_panel_progression',
			'type' => 'text',
			'priority'   => 9,
	    )
	);
	
	

	// Add Copyright Text
	$wp_customize->add_setting(
	    'copyright_textbox',
	    array(
	        'default' => '&copy; 2014 All Rights Reserved. Developed by ProgressionStudios',
	    )
	);

	$wp_customize->add_control(new ProgressionTextAreaControl( $wp_customize, 'copyright_textbox', array( 
		'label' => __('Copyright Text', 'progression'), 
		'section' => 'options_panel_progression', 
		'settings' => 'copyright_textbox', 
		'priority' => 10, ) 
		)
	);
	
	
	//Footer Column
	$wp_customize->add_setting( 
		'footer_cols' ,
		array(
	        'default' => '4',
	    )
	);
	$wp_customize->add_control(
   'footer_cols',
   	array(
	   	'label' => __('Footer Column Count (1-4)', 'progression'), 
			'section' => 'options_panel_progression',
			'type' => 'text',
			'priority'   => 15,
	    )
	);
	
	
	//Comment Options
	$wp_customize->add_setting( 
		'comment_progression' 
	);
	$wp_customize->add_control(
   'comment_progression',
   	array(
	   	'label' => __('Display comments for pages?', 'progression'), 
			'section' => 'options_panel_progression',
			'type' => 'checkbox',
			'priority'   => 11,
	    )
	);
	
	//Comment Options
	$wp_customize->add_setting( 
		'sidebar_right_pro' 
	);
	$wp_customize->add_control(
   'sidebar_right_pro',
   	array(
	   	'label' => __('Display sidebar on right?', 'progression'), 
			'section' => 'options_panel_progression',
			'type' => 'checkbox',
			'priority'   => 15,
	    )
	);
	
	
	
	
	//Shop Column Count
	$wp_customize->add_setting( 
		'shop_col_progression' ,
		array(
	        'default' => '3',
	    )
	);
	$wp_customize->add_control(
   'shop_col_progression',
   	array(
	   	'label' => __('Shop posts per column (2-4)', 'progression'), 
			'section' => 'options_panel_progression',
			'type' => 'text',
			'priority'   => 50,
	    )
	);
	


	//Add Section Page of Theme Settings
	    $wp_customize->add_section(
	        'home_options_progression',
	        array(
	            'title' => __('Slider Settings', 'progression'),
	            'description' => __('Slider Theme Settings', 'progression'),
	            'priority' => 40,
	        )
	    );
		

	
	//Slider AutoPLay
	$wp_customize->add_setting( 
		'slide_transition' ,
		array(
	        'default' => 'fade',
	    )
	);
	$wp_customize->add_control(
   'slide_transition',
   	array(
	   	'label' => __('Slider Transition', 'progression'), 
			'section' => 'home_options_progression',
			'type' => 'select',
			'priority'   => 23,
			'choices'    => array(							
			        'fade' => 'Fade',
			        'slide' => 'Slide',
			    ),
	    )
	);
	
	
	
	//Slider AutoPLay
	$wp_customize->add_setting( 
		'slide_autoplay' ,
		array(
	        'default' => 'true',
	    )
	);
	$wp_customize->add_control(
   'slide_autoplay',
   	array(
	   	'label' => __('Slider AutoPlay', 'progression'), 
			'section' => 'home_options_progression',
			'type' => 'select',
			'priority'   => 25,
			'choices'    => array(							
			        'true' => 'On',
			        'false' => 'Off',
			    ),
	    )
	);
	
	//Slider AutoPLay Timer
	$wp_customize->add_setting( 
		'slide_autoplay_timer' ,
		array(
	        'default' => '7500',
	    )
	);
	$wp_customize->add_control(
   'slide_autoplay_timer',
   	array(
	   	'label' => __('Slider AutoPlay Timer (in milliseconds)', 'progression'), 
			'section' => 'home_options_progression',
			'type' => 'text',
			'priority'   => 28,
	    )
	);
	
	//Slider Next/Previous
	$wp_customize->add_setting( 
		'slide_nextprev' ,
		array(
	        'default' => 'true',
	    )
	);
	$wp_customize->add_control(
   'slide_nextprev',
   	array(
	   	'label' => __('Slider Next/Previous Arrows', 'progression'), 
			'section' => 'home_options_progression',
			'type' => 'select',
			'priority'   => 30,
			'choices'    => array(							
			        'true' => 'On',
			        'false' => 'Off',
			    ),
	    )
	);
	
	
	
	//Slider Thumbnails
	$wp_customize->add_setting( 
		'slide_thumbs' ,
		array(
	        'default' => 'true',
	    )
	);
	$wp_customize->add_control(
   'slide_thumbs',
   	array(
	   	'label' => __('Slider Thumbnail Navigation', 'progression'), 
			'section' => 'home_options_progression',
			'type' => 'select',
			'priority'   => 35,
			'choices'    => array(							
			        'true' => 'On',
			        'false' => 'Off',
			    ),
	    )
	);

//Add Section Page of Footer Settings
    $wp_customize->add_section(
        'footer_options_progression',
        array(
            'title' => __('Social Icon Settings', 'progression'),
            'description' => 'Footer Settings here!',
            'priority' => 45,
        )
    );
	

	
	//Twitter Icon
	$wp_customize->add_setting( 
		'twitter_link_progression'
	);
	$wp_customize->add_control(
   'twitter_link_progression',
   	array(
	   	'label' => __('Twitter Icon Link', 'progression'), 
			'section' => 'footer_options_progression',
			'type' => 'text',
			'priority'   => 10,
	    )
	);
	
	//Facebook Icon
	$wp_customize->add_setting( 
		'facebook_link_progression'
	);
	$wp_customize->add_control(
   'facebook_link_progression',
   	array(
	   	'label' => __('Facebook Icon Link', 'progression'), 
			'section' => 'footer_options_progression',
			'type' => 'text',
			'priority'   => 14,
	    )
	);
	
	
	//Facebook Icon
	$wp_customize->add_setting( 
		'vimeo_link_progression'
	);
	$wp_customize->add_control(
   'vimeo_link_progression',
   	array(
	   	'label' => __('Vimeo Icon Link', 'progression'), 
			'section' => 'footer_options_progression',
			'type' => 'text',
			'priority'   => 16,
	    )
	);
	
	//YouTube Icon
	$wp_customize->add_setting( 
		'youtube_link_progression'
	);
	$wp_customize->add_control(
   'youtube_link_progression',
   	array(
	   	'label' => __('YouTube Icon Link', 'progression'), 
			'section' => 'footer_options_progression',
			'type' => 'text',
			'priority'   => 18,
	    )
	);
	
	//Google Plus Icon
	$wp_customize->add_setting( 
		'google_link_progression'
	);
	$wp_customize->add_control(
   'google_link_progression',
   	array(
	   	'label' => __('Google Plus Icon Link', 'progression'), 
			'section' => 'footer_options_progression',
			'type' => 'text',
			'priority'   => 22,
	    )
	);
	
	
	//Pinterest Icon
	$wp_customize->add_setting( 
		'pinterest_link_progression'
	);
	$wp_customize->add_control(
   'pinterest_link_progression',
   	array(
	   	'label' => __('Pinterest Icon Link', 'progression'), 
			'section' => 'footer_options_progression',
			'type' => 'text',
			'priority'   => 24,
	    )
	);
	
	
	//Dribbble Icon
	$wp_customize->add_setting( 
		'dribbble_link_progression'
	);
	$wp_customize->add_control(
   'dribbble_link_progression',
   	array(
	   	'label' => __('Dribbble Icon Link', 'progression'), 
			'section' => 'footer_options_progression',
			'type' => 'text',
			'priority'   => 26,
	    )
	);
	
	
	//Linkedin Icon
	$wp_customize->add_setting( 
		'linkedin_link_progression'
	);
	$wp_customize->add_control(
   'linkedin_link_progression',
   	array(
	   	'label' => __('LinkedIn Icon Link', 'progression'), 
			'section' => 'footer_options_progression',
			'type' => 'text',
			'priority'   => 28,
	    )
	);
	
	
	//Flickr Icon
	$wp_customize->add_setting( 
		'flickr_link_progression'
	);
	$wp_customize->add_control(
   'flickr_link_progression',
   	array(
	   	'label' => __('Flickr Icon Link', 'progression'), 
			'section' => 'footer_options_progression',
			'type' => 'text',
			'priority'   => 30,
	    )
	);
	
	
	
	
	//Instagram Icon
	$wp_customize->add_setting( 
		'instagram_link_progression'
	);
	$wp_customize->add_control(
   'instagram_link_progression',
   	array(
	   	'label' => __('Instagram Icon Link', 'progression'), 
			'section' => 'footer_options_progression',
			'type' => 'text',
			'priority'   => 35,
	    )
	);
	
	
	
	//Tumblr Icon
	$wp_customize->add_setting( 
		'tumblr_link_progression'
	);
	$wp_customize->add_control(
   'tumblr_link_progression',
   	array(
	   	'label' => __('Tumblr Icon Link', 'progression'), 
			'section' => 'footer_options_progression',
			'type' => 'text',
			'priority'   => 37,
	    )
	);

	
	
	
	

//Add Section Page of Background Colors
    $wp_customize->add_section(
        'progression_background_colors',
        array(
            'title' => __('Background Colors', 'progression'),
            'description' => 'Adjust background colors for the theme!',
            'priority' => 50,
        )
    );
	
	$wp_customize->add_setting('header_top_bg_pro', array(
	    'default'     => '#16252c'
	));
	
	
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'header_top_bg_pro', array(
		'label'        => __( 'Top Header Background Color', 'progression' ),
		'section'    => 'progression_background_colors',
		'settings'   => 'header_top_bg_pro',
		'priority'   => 2,
	)));
	
	$wp_customize->add_setting('header_top_border_pro', array(
	    'default'     => '#253f4b'
	));
	
	
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'header_top_border_pro', array(
		'label'        => __( 'Header Border Color', 'progression' ),
		'section'    => 'progression_background_colors',
		'settings'   => 'header_top_border_pro',
		'priority'   => 3,
	)));
	
	$wp_customize->add_setting('header_bg', array(
	    'default'     => '#16252c'
	));
	
	
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'header_bg', array(
		'label'        => __( 'Header Background Color', 'progression' ),
		'section'    => 'progression_background_colors',
		'settings'   => 'header_bg',
		'priority'   => 5,
	)));
	
	$wp_customize->add_setting('nav_bg', array(
	    'default'     => '#16252c'
	));
	
	
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'nav_bg', array(
		'label'        => __( 'Sub-Navigation Background Color', 'progression' ),
		'section'    => 'progression_background_colors',
		'settings'   => 'nav_bg',
		'priority'   => 8,
	)));
	
	
	
	$wp_customize->add_setting('panel_search_pro', array(
	    'default'     => '#478484'
	));
	
	
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'panel_search_pro', array(
		'label'        => __( 'Header Search Background Color', 'progression' ),
		'section'    => 'progression_background_colors',
		'settings'   => 'panel_search_pro',
		'priority'   => 11,
	)));
	

	
	$wp_customize->add_setting('page_title_bg', array(
	    'default'     => '#b45947'
	));
	
	
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'page_title_bg', array(
		'label'        => __( 'Page Title Background Color', 'progression' ),
		'section'    => 'progression_background_colors',
		'settings'   => 'page_title_bg',
		'priority'   => 12,
	)));
	
	$wp_customize->add_setting('body_bg_progression', array(
	    'default'     => '#fcfcfc'
	));
	
	
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'body_bg_progression', array(
		'label'        => __( 'Body Background Color', 'progression' ),
		'section'    => 'progression_background_colors',
		'settings'   => 'body_bg_progression',
		'priority'   => 15,
	)));
	
	
	
	$wp_customize->add_setting('footer_highlight_pro', array(
	    'default'     => '#8bca81'
	));
	
	
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'footer_highlight_pro', array(
		'label'        => __( 'Footer Top Widget Background Color', 'progression' ),
		'section'    => 'progression_background_colors',
		'settings'   => 'footer_highlight_pro',
		'priority'   => 16,
	)));
	


	
	$wp_customize->add_setting('footer_widget_pro', array(
	    'default'     => '#16242c'
	));
	
	
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'footer_widget_pro', array(
		'label'        => __( 'Footer Widget Background Color', 'progression' ),
		'section'    => 'progression_background_colors',
		'settings'   => 'footer_widget_pro',
		'priority'   => 19,
	)));
	
	
	$wp_customize->add_setting('footer_bg_progression', array(
	    'default'     => '#142128'
	));
	
	
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'footer_bg_progression', array(
		'label'        => __( 'Footer Background Color', 'progression' ),
		'section'    => 'progression_background_colors',
		'settings'   => 'footer_bg_progression',
		'priority'   => 20,
	)));
	
	
	
	
	
	$wp_customize->add_setting('button_bg_progression', array(
	    'default'     => '#68b95a'
	));
	
	
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'button_bg_progression', array(
		'label'        => __( 'Button Background Color', 'progression' ),
		'section'    => 'progression_background_colors',
		'settings'   => 'button_bg_progression',
		'priority'   => 25,
	)));
	
	
	
	
	$wp_customize->add_setting('button_hover_bg_progression', array(
	    'default'     => '#404040'
	));
	
	
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'button_hover_bg_progression', array(
		'label'        => __( 'Button Hover Background Color', 'progression' ),
		'section'    => 'progression_background_colors',
		'settings'   => 'button_hover_bg_progression',
		'priority'   => 30,
	)));
	


//Add Section Page of Background Colors
    $wp_customize->add_section(
        'progression_font_colors',
        array(
            'title' => __('Font Colors', 'progression'),
            'description' => 'Adjust font colors for the theme!',
            'priority' => 55,
        )
    );
	
	
	$wp_customize->add_setting('header_font_pro', array(
	    'default'     => '#cdeeff'
	));
	
	
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'header_font_pro', array(
		'label'        => __( 'Top Header Font Color', 'progression' ),
		'section'    => 'progression_font_colors',
		'settings'   => 'header_font_pro',
		'priority'   => 2,
	)));
	
	
	$wp_customize->add_setting('header_font_hover_pro', array(
	    'default'     => '#ffffff'
	));
	
	
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'header_font_hover_pro', array(
		'label'        => __( 'Top Header Hover Font Color', 'progression' ),
		'section'    => 'progression_font_colors',
		'settings'   => 'header_font_hover_pro',
		'priority'   => 3,
	)));
	
	$wp_customize->add_setting('body_font_progression', array(
	    'default'     => '#787878'
	));
	
	
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'body_font_progression', array(
		'label'        => __( 'Body Font Color', 'progression' ),
		'section'    => 'progression_font_colors',
		'settings'   => 'body_font_progression',
		'priority'   => 5,
	)));
	
	
	$wp_customize->add_setting('body_link_progression', array(
	    'default'     => '#66b958'
	));
	
	
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'body_link_progression', array(
		'label'        => __( 'Main Link Color', 'progression' ),
		'section'    => 'progression_font_colors',
		'settings'   => 'body_link_progression',
		'priority'   => 7,
	)));
	
	
	$wp_customize->add_setting('body_link_hover_progression', array(
	    'default'     => '#404040'
	));
	
	
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'body_link_hover_progression', array(
		'label'        => __( 'Main Hover Link Color', 'progression' ),
		'section'    => 'progression_font_colors',
		'settings'   => 'body_link_hover_progression',
		'priority'   => 9,
	)));
	
	$wp_customize->add_setting('navigation_menu_color', array(
	    'default'     => '#cbb98e'
	));
	
	
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'navigation_menu_color', array(
		'label'        => __( 'Navigation Link Color', 'progression' ),
		'section'    => 'progression_font_colors',
		'settings'   => 'navigation_menu_color',
		'priority'   => 12,
	)));
	
	
	$wp_customize->add_setting('navigation_selected_color', array(
	    'default'     => '#ffffff'
	));
	
	
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'navigation_selected_color', array(
		'label'        => __( 'Navigation Selected Color', 'progression' ),
		'section'    => 'progression_font_colors',
		'settings'   => 'navigation_selected_color',
		'priority'   => 13,
	)));
	
	
	
	
	$wp_customize->add_setting('sub_font_color', array(
	    'default'     => '#cccccc'
	));
	
	
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'sub_font_color', array(
		'label'        => __( 'Sub-Navigation Link Color', 'progression' ),
		'section'    => 'progression_font_colors',
		'settings'   => 'sub_font_color',
		'priority'   => 14,
	)));
	
	$wp_customize->add_setting('sub_font_hover_color', array(
	    'default'     => '#ffffff'
	));
	
	
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'sub_font_hover_color', array(
		'label'        => __( 'Sub-Navigation Hover Color', 'progression' ),
		'section'    => 'progression_font_colors',
		'settings'   => 'sub_font_hover_color',
		'priority'   => 15,
	)));
	
	$wp_customize->add_setting('heading_font_progression', array(
	    'default'     => '#262626'
	));
	
	
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'heading_font_progression', array(
		'label'        => __( 'Heading Font Color', 'progression' ),
		'section'    => 'progression_font_colors',
		'settings'   => 'heading_font_progression',
		'priority'   => 18,
	)));
	
	
	$wp_customize->add_setting('button_font_pro', array(
	    'default'     => '#ffffff'
	));
	
	
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'button_font_pro', array(
		'label'        => __( 'Button Font Color', 'progression' ),
		'section'    => 'progression_font_colors',
		'settings'   => 'button_font_pro',
		'priority'   => 20,
	)));
	
	
	$wp_customize->add_setting('button_hover_pro', array(
	    'default'     => '#ffffff'
	));
	
	
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'button_hover_pro', array(
		'label'        => __( 'Button Hover Font Color', 'progression' ),
		'section'    => 'progression_font_colors',
		'settings'   => 'button_hover_pro',
		'priority'   => 22,
	)));
	


}
add_action( 'customize_register', 'progression_customizer' );


function progression_customize_css()
{
    ?>
 
<style type="text/css">
	body #logo, body #logo img {width:<?php echo get_theme_mod('logo_width', '165'); ?>px;}
	<?php if (get_theme_mod( 'comment_progression')) : ?><?php else: ?>body.page #comments {display:none;}<?php endif ?>
	.sf-menu a {margin-top:<?php echo get_theme_mod('header_padding', '36'); ?>px; margin-bottom:<?php echo get_theme_mod('header_padding', '36'); ?>px;}
	#header-top {background-color:<?php echo get_theme_mod('header_top_bg_pro', '#16252c'); ?>; border-bottom:1px solid <?php echo get_theme_mod('header_top_border_pro', '#253f4b'); ?>;}
	#subscribe-right {border-right:1px solid <?php echo get_theme_mod('header_top_border_pro', '#253f4b'); ?>;}
	.sf-menu ul {background-color:<?php echo get_theme_mod('nav_bg', '#16252c'); ?>;}
	header, #search-container-pro {background-color:<?php echo get_theme_mod('header_bg', '#16252c'); ?>;}
	#page-title, #single-product-pro {background-color:<?php echo get_theme_mod('page_title_bg', '#b45947'); ?>;}
	#main {background-color:<?php echo get_theme_mod('body_bg_progression', '#fcfcfc'); ?>;}
	#featured-assistance-pro {background-color:<?php echo get_theme_mod('footer_highlight_pro', '#8bca81'); ?>;}
	#widget-area {background-color:<?php echo get_theme_mod('footer_widget_pro', '#16242c'); ?>;}
	body, footer {background-color:<?php echo get_theme_mod('footer_bg_progression', '#142128'); ?>;}
	.search-form input.search-submit, body #main a.button, body #main button.single_add_to_cart_button, body #main input.button, body.woocommerce-cart #main input.button.checkout-button, body #main button.button, body #single-product-pro a.button, 
	body #single-product-pro button.single_add_to_cart_button, body #single-product-pro input.button, body.woocommerce-cart #single-product-pro input.button.checkout-button, body #single-product-pro button.button,
	.progression-button, body input.wpcf7-submit, body a.more-link, .page-numbers span.current, .page-numbers a:hover,
	body a.ls-sc-button.default, body #main .width-container input#submit, body  #main .widget_price_filter .ui-slider .ui-slider-range,
	body #searchform input#searchsubmit  { background:<?php echo get_theme_mod('button_bg_progression', '#68b95a'); ?>; color:<?php echo get_theme_mod('button_font_pro', '#ffffff'); ?>;}
	.page-numbers span, .page-numbers a {
		border-right:1px solid <?php echo get_theme_mod('button_bg_progression', '#68b95a'); ?>;
		border-top:1px solid <?php echo get_theme_mod('button_bg_progression', '#68b95a'); ?>;
		border-left:1px solid <?php echo get_theme_mod('button_bg_progression', '#68b95a'); ?>;
		border-bottom:2px solid <?php echo get_theme_mod('button_bg_progression', '#68b95a'); ?>;
	}
	#panel-search {background-color:<?php echo get_theme_mod('panel_search_pro', '#478484'); ?>;}
	body #searchform input#searchsubmit:hover, 
	.search-form input.search-submit:hover, body #main a.button:hover, body #main button.single_add_to_cart_button:hover, body #main input.button:hover, body.woocommerce-cart #main input.button.checkout-button:hover, body #main button.button:hover, body #single-product-pro a.button:hover, body #single-product-pro button.single_add_to_cart_button:hover, body #single-product-pro input.button:hover, body.woocommerce-cart #single-product-pro input.button.checkout-button:hover, body #single-product-pro button.button:hover,
	.progression-button:hover, body #main .width-container input#submit:hover, body input.wpcf7-submit:hover, body a.more-link:hover, body a.ls-sc-button.default:hover,
	body #main #sidebar button.button:hover, body #main #sidebar a.button:hover
	 {background:<?php echo get_theme_mod('button_hover_bg_progression', '#404040'); ?>; color:<?php echo get_theme_mod('button_hover_pro', '#ffffff'); ?>;}
	 body {color:<?php echo get_theme_mod('body_font_progression', '#787878'); ?>;}
	 #header-top, #header-top a {color:<?php echo get_theme_mod('header_font_pro', '#cdeeff'); ?>;}
	 #header-top a:hover {color:<?php echo get_theme_mod('header_font_hover_pro', '#ffffff'); ?>;}
	 a, .button:before, #sidebar a:hover, #sidebar li.current-cat a, #sidebar li.current-cat-parent a, #sidebar li.current-cat, #sidebar li.current-cat-parent, #sidebar li.current-cat-parent li.current-cat,  #sidebar li.current-cat-parent li.current-cat a,
	 #sidebar li.current-cat li a:hover, #sidebar li.current-cat-parent li a:hover
	  {color:<?php echo get_theme_mod('body_link_progression', '#66b958'); ?>;}
	 a:hover {color:<?php echo get_theme_mod('body_link_hover_progression', '#404040'); ?>;}
	 .sf-menu a, #cart-icon-pro i, #cart-icon-pro a {color:<?php echo get_theme_mod('navigation_menu_color', '#cbb98e'); ?>;}
	 #cart-icon-pro a:hover, #cart-icon-pro a:hover i, .sf-menu li.current-menu-item a, .sf-menu li.current-menu-item a:visited, .sf-menu a:hover, .sf-menu li a:hover, .sf-menu a:hover, .sf-menu a:visited:hover, .sf-menu li.sfHover a, .sf-menu li.sfHover a:visited {
	 	color:<?php echo get_theme_mod('navigation_selected_color', '#ffffff'); ?>;
	 }
	 .sf-menu li.sfHover li a, .sf-menu li.sfHover li a:visited, .sf-menu li.sfHover li li a, .sf-menu li.sfHover li li a:visited, .sf-menu li.sfHover li li li a, .sf-menu li.sfHover li li li a:visited, .sf-menu li.sfHover li li li li a, .sf-menu li.sfHover li li li li a:visited {
	 	color:<?php echo get_theme_mod('sub_font_color', '#cccccc'); ?>;
	 }
	 .sf-menu li li:hover, .sf-menu li li.sfHover, .sf-menu li li a:focus, .sf-menu li li a:hover, .sf-menu li li a:active, .sf-menu li li.sfHover a, .sf-menu li.sfHover li a:visited:hover, .sf-menu li li:hover a:visited,
	 .sf-menu li li li:hover, .sf-menu li li li.sfHover, .sf-menu li li li a:focus, .sf-menu li li li a:hover, .sf-menu li li li a:active, .sf-menu li li li.sfHover a, .sf-menu li li.sfHover li a:visited:hover, .sf-menu li li li:hover a:visited,
	 .sf-menu li li li li:hover, .sf-menu li li li li.sfHover, .sf-menu li li li li a:focus, .sf-menu li li li li a:hover, .sf-menu li li li li a:active, .sf-menu li li li li.sfHover a, .sf-menu li li li.sfHover li a:visited:hover, .sf-menu li li li li:hover a:visited,
	 .sf-menu li li li li li:hover, .sf-menu li li li li li.sfHover, .sf-menu li li li li li a:focus, .sf-menu li li li li li a:hover, .sf-menu li li li li li a:active, .sf-menu li li li li li.sfHover a, .sf-menu li li li li.sfHover li a:visited:hover, .sf-menu li li li li li:hover a:visited  {
	 	color:<?php echo get_theme_mod('sub_font_hover_color', '#ffffff'); ?>;
	 }
	h1, h2, h3, h4, h5, h6, h1 a, h2 a, h3 a, h4 , h5 a, h6 a {color:<?php echo get_theme_mod('heading_font_progression', '#262626'); ?>;}
	<?php if (get_theme_mod( 'sidebar_right_pro')) : ?>
	body #content-container {float:left;} body #sidebar {float:right;}
	#main .page-sidebar-pro {  background:url("<?php echo get_template_directory_uri(); ?>/images/sidebar-bg-right.png") top right repeat-y;}
	@media only screen and (min-width: 768px) and (max-width: 959px) {
		#main .page-sidebar-pro { background-image:url("<?php echo get_template_directory_uri(); ?>/images/sidebar-bg-right-767.png");}
		} <?php else: ?><?php endif ?>
</style>
    <?php
}
add_action('wp_head', 'progression_customize_css');

