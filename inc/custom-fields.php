<?php
function add_custom_meta_boxes() {
    $meta_box = array(
        'id'         => 'progression_page_settings', // Meta box ID
        'title'      => 'Page Settings', // Meta box title
        'pages'      => array('page'), // Post types this meta box should be shown on
        'context'    => 'normal', // Meta box context
        'priority'   => 'high', // Meta box priority
        'fields' => array(
            array(
                'id' => 'progression_category_slug',
                'name' => 'Homepage Slider: Insert Slider Slug',
                'desc' => '<br>Choose what slider category slug you want to display on this page (Homepage only). ',
                'type' => 'text',
                'std' => ''
            )
        )
    );
    dev7_add_meta_box( $meta_box );
	
    $meta_box2 = array(
        'id'         => 'progression_post_settings', // Meta box ID
        'title'      => 'Post Settings', // Meta box title
        'pages'      => array('post'), // Post types this meta box should be shown on
        'context'    => 'normal', // Meta box context
        'priority'   => 'high', // Meta box priority
        'fields' => array(
            array(
                'id' => 'progression_media_embed',
                'name' => 'Audio/Video Embed',
                'desc' => '<br>Paste in your video embed code here',
                'type' => 'textarea',
                'std' => ''
            ),
            array(
                'id' => 'progression_external_link',
                'name' => 'External Link',
                'desc' => '<br>Make your post link to another page than the post page. ',
                'type' => 'text',
                'std' => ''
            )
        )
    );
    dev7_add_meta_box( $meta_box2 );
	
	
    $meta_box3 = array(
        'id'         => 'progression_post_settings_slider', // Meta box ID
        'title'      => 'Slider Settings', // Meta box title
        'pages'      => array('portfolio'), // Post types this meta box should be shown on
        'context'    => 'normal', // Meta box context
        'priority'   => 'high', // Meta box priority
        'fields' => array(
			array(
                'id' => 'progression_caption',
                'name' => 'Display Caption',
                'desc' => '<br>Easily add/remove the caption.',
                'type' => 'select',
                'std' => 'enable',
                'choices' => array(
                    'enable' => 'Enable',
                    'disable' => 'Disable'
                )
            ),
			array(
                'id' => 'progression_caption_alignment',
                'name' => 'Caption Alignment',
                'desc' => '<br>Easily adjust the caption to the left or right.',
                'type' => 'select',
                'std' => 'left-align',
                'choices' => array(
                    'left-align' => 'Left Align',
					'center-align' => 'Center Align',
                    'right-align' => 'Right Align'
                )
            ),
			array(
                'id' => 'progression_cap_bg',
                'name' => 'Caption Backgrounnd',
                'desc' => '<br>Adjust the caption background color.',
                'type' => 'select',
                'std' => 'enable',
                'choices' => array(
                    'light-color' => 'Light',
                    'light-dark' => 'Dark'
                )
            ),
            array(
                'id' => 'progression_link',
                'name' => 'Slider Heading/Image Link',
                'desc' => '<br>Choose a link for your slider Image (Leave blank for no link). ',
                'type' => 'text',
                'std' => ''
            )
        )
    );
    dev7_add_meta_box( $meta_box3 );
}
add_action( 'dev7_meta_boxes', 'add_custom_meta_boxes' );






?>