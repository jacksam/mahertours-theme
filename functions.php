<?php
/**
 * progression functions and definitions
 *
 * @package progression
 * @since progression 1.0
 */


// Post Thumbnails
add_theme_support('post-thumbnails');
// add_image_size('featured-image-scale-crop', 787, 300, true);
add_image_size('progression-slider', 1400, 575, true);
add_image_size('progression-blog', 900, 400, true); // Blog Post Image
/*add_image_size('progression-portfolio-thumb', 600, 1500, false);   //600 wide x 1500 tall without any cropping
add_image_size('progression-portfolio-slider', 600, 400, true);   //600 wide x 1500 tall without any cropping
add_image_size('progression-portfolio-single', 2500, 800, false);   //2500 wide x 800 tall without any cropping*/
add_image_size('progression-slider-thumb', 100, 75, true);  //100 wide by 75 tall Image is cropped due to true setting
add_image_size('blog-home', 360, 280, true);

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since progression 1.0
 */
if ( ! isset( $content_width ) )
	$content_width = 640; /* pixels */


if ( ! function_exists( 'progression_setup' ) ):
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 *
 * @since progression 1.0
 */
function progression_setup() {

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on progression, use a find and replace
	 * to change 'progression' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'progression', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );
	
	// Include widgets
	require( get_template_directory() . '/widgets/widgets.php' );
	
	
	/**
	 * Enable support for Post Formats
	 */
	add_theme_support( 'post-formats', array( 'gallery', 'video', 'audio', 'link' ) );
	
	
	
	require_once(TEMPLATEPATH . '/tgm-plugin-activation/plugin-activation.php');
	
	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'progression' ),
	) );



}
endif; // progression_setup
add_action( 'after_setup_theme', 'progression_setup' );



/* WooCommerce */
add_theme_support( 'woocommerce' );
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);


function woo_related_products_limit() {
  global $product;
	
	$args['posts_per_page'] = 3;
	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
  function jk_related_products_args( $args ) {
 
	$args['posts_per_page'] = 3; // 4 related products
	$args['columns'] = 2; // arranged in 2 columns
	return $args;
}

// Display 24 products per page. Goes in functions.php
add_filter('loop_shop_per_page', create_function('$cols', 'return 12;'));



//adjust tag cloud


/**
 * Registering Custom Post Type
 */
add_action('init', 'progression_portfolio_init');
function progression_portfolio_init() {
	register_post_type(
		'portfolio',
		array(
			'labels' => array(
				'name' => 'Slides',
				'singular_name' => 'Slide'
			),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'portfolio-type'),
			'supports' => array('title', 'editor', 'thumbnail','comments', 'revisions'),
			'can_export' => true,
		)
	);

	register_taxonomy('portfolio_type', 'portfolio', array('hierarchical' => true, 'label' => 'Slider Categories', 'query_var' => true, 'rewrite' => true));
}


/**
 * Register widgetized area and update sidebar with default widgets
 *
 * @since progression 1.0
 */
function progression_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Sidebar', 'progression' ),
		'description' => __( 'Main Sidebar', 'progression' ),
		'id' => 'sidebar-1',
		'before_widget' => '<div id="%1$s" class="sidebar-item widget %2$s">',
		'after_widget' => '<div class="sidebar-divider"></div></div>',
		'before_title' => '<h6 class="widget-title">',
		'after_title' => '</h6>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Shop Sidebar', 'progression' ),
		'description' => __( 'WooCommerce Sidebar', 'progression' ),
		'id' => 'sidebar-shop',
		'before_widget' => '<div id="%1$s" class="sidebar-item widget %2$s">',
		'after_widget' => '<div class="sidebar-divider"></div></div>',
		'before_title' => '<h6 class="widget-title">',
		'after_title' => '</h6>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Wide Footer Widget', 'progression' ),
		'description' => __( 'Footer area at bottom of pages', 'progression' ),
		'id' => 'footer-top-widgets',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="title-homepage">',
		'after_title' => '</h3>',
	) );

	
	register_sidebar( array(
		'name' => __( 'Footer Widgets', 'progression' ),
		'description' => __( 'Footer widgets', 'progression' ),
		'id' => 'footer-widgets',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h5 class="widget-title">',
		'after_title' => '</h5>',
	) );

}
add_action( 'widgets_init', 'progression_widgets_init' );



/* remove more link jump */
function remove_more_link_scroll( $link ) {
	$link = preg_replace( '|#more-[0-9]+|', '', $link );
	return $link;
}
add_filter( 'the_content_more_link', 'remove_more_link_scroll' );


/* custom portfolio posts per page */
function my_post_queries( $query ) {
	$portfolio_count = get_theme_mod('portfolio_pages_progression');
	if(is_tax( 'portfolio_type' )){
      // show 50 posts on custom taxonomy pages
      $query->set('posts_per_page', $portfolio_count);
    }
	

	if(is_post_type_archive( 'portfolio' )){
      $query->set('posts_per_page', $portfolio_count);
    }
	
  }
add_action( 'pre_get_posts', 'my_post_queries' );



/**
 * Enqueue scripts and styles
 */
function progression_scripts() {
	wp_enqueue_style( 'progression-style', get_stylesheet_uri() );
	wp_enqueue_style( 'custom', get_template_directory_uri() . '/css/custom.css', array( 'progression-style' ) );
	wp_enqueue_style( 'responsive', get_template_directory_uri() . '/css/responsive.css', array( 'progression-style' ) );
	wp_enqueue_style( 'chosen', get_template_directory_uri() . '/css/chosen.css', array( 'progression-style' ) );
	wp_enqueue_style( 'responsive-tabs', get_template_directory_uri() . '/css/responsive-tabs.css', array( 'progression-style' ) );
	wp_enqueue_style( 'google-fonts', 'http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700|Cabin+Condensed:400,700', array( 'progression-style' ) );

	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/libs/modernizr-2.6.2.min.js', false, '20120206', false );
	wp_enqueue_script( 'chosenjs', get_template_directory_uri() . '/js/libs/chosen.jquery.min.js', array( 'jquery' ), '20120206', false );
	wp_enqueue_script( 'responsive-tabs-js', get_template_directory_uri() . '/js/libs/jquery.responsiveTabs.min.js', array( 'jquery' ), '20120206', false );
	wp_enqueue_script( 'plugins', get_template_directory_uri() . '/js/plugins.js', array( 'jquery' ), '20120206', true );
	wp_enqueue_script( 'scripts', get_template_directory_uri() . '/js/script.js', array( 'jquery' ), '20120206', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	
}
add_action( 'wp_enqueue_scripts', 'progression_scripts' );

function progression_js_insert()
{
    ?>
	
	<?php if( is_page_template('homepage.php') || is_page_template('homepage-v2.php') ): ?>	
		<script type="text/javascript">
		jQuery(document).ready(function($) {
		    $('#homepage-slider').flexslider({
				animation: "<?php echo get_theme_mod('slide_transition', 'fade'); ?>",              //String: Select your animation type, "fade" or "slide"
				slideshow: <?php echo get_theme_mod('slide_autoplay', 'true'); ?>,                //Boolean: Animate slider automatically
				slideshowSpeed: <?php echo get_theme_mod('slide_autoplay_timer', '7500'); ?>,           //Integer: Set the speed of the slideshow cycling, in milliseconds
				animationDuration: 500,         //Integer: Set the speed of animations, in milliseconds
				directionNav:  <?php echo get_theme_mod('slide_nextprev', 'true'); ?>,             //Boolean: Create navigation for previous/next navigation? (true/false)
				controlNav: <?php echo get_theme_mod('slide_thumbs', 'true'); ?>,               //Boolean: Create navigation for paging control of each clide? Note: Leave true for manualControls usage
				keyboardNav: true,              //Boolean: Allow slider navigating via keyboard left/right keys
				mousewheel: false,              //Boolean: Allow slider navigating via mousewheel
				useCSS: true,
				animationLoop: true,            //Boolean: Should the animation loop? If false, directionNav will received "disable" classes at either end
				pauseOnAction: true,            //Boolean: Pause the slideshow when interacting with control elements, highly recommended.
				pauseOnHover: false            //Boolean: Pause the slideshow when hovering over slider, then resume when no longer hovering
		    });
		});
		</script>
	<?php endif; ?>
	
 	<?php
}
add_action('wp_footer', 'progression_js_insert');



/*
	MetaBox Options from Dev7studios
*/
require get_template_directory() . '/inc/dev7_meta_box_framework.php';
require get_template_directory() . '/inc/custom-fields.php';


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Change 'Add to cart' button to 'Read more'.
 */
add_filter( 'woocommerce_loop_add_to_cart_link', 'change_add_to_cart_loop' );
function change_add_to_cart_loop( $product ) {
	global $product; // this may not be necessary as it should have pulled the object in already
	return '<a href="' . esc_url( $product->get_permalink( $product->id ) ) . '" class="button add_to_cart_button product_type_simple">Read more</a>';
}
function remove_loop_button(){
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
}
add_action('init','remove_loop_button');


// add taxonomy term of products category to body classes
function woo_custom_taxonomy_in_body_class( $classes ){
	if( is_singular( 'product' ) ) {
		$custom_terms = get_the_terms(0, 'product_cat');
			if ($custom_terms) {
				foreach ($custom_terms as $custom_term) {
				$classes[] = 'product_cat_' . $custom_term->slug;
			}
		}
	}
	return $classes;
}
add_filter( 'body_class', 'woo_custom_taxonomy_in_body_class' );

add_filter( 'woocommerce_product_description_heading', 'remove_product_description_heading' );
function remove_product_description_heading() {
return '';
}


add_filter( 'wpcf7_mail_components', 'remove_blank_lines' );
function remove_blank_lines( $mail ) {
	if ( is_array( $mail ) && ! empty( $mail['body'] ) )
		$mail['body'] = preg_replace( '|\n\s*\n|', "\n\n", $mail['body'] );
	return $mail;
}


// Changing excerpt length
function custom_excerpt_length( $length ) {
	return 29;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


// Changing excerpt more
function new_excerpt_more($more) {
	global $post;
	return '… <a href="'. get_permalink($post->ID) . '">' . 'Read More &raquo;' . '</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

	
if(false === get_option("medium_crop")) {
   add_option("medium_crop", "1");
} else {
   update_option("medium_crop", "1");
}

add_filter('widget_text', 'do_shortcode');

add_filter( 'wpcf7_support_html5_fallback', '__return_true' );

add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
function woo_rename_tabs( $tabs ) {
	$tabs['description']['title'] = __( 'Overview' );		// Rename the description tab
	return $tabs;
}

remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

add_action( 'admin_init', 'my_remove_menu_pages' );
function my_remove_menu_pages() {
    global $user_ID;
    if ( current_user_can( 'editor' ) ) {
		remove_menu_page('tools.php'); // Tools
		remove_menu_page('options-general.php'); // Settings
		remove_menu_page( 'edit.php?post_type=product_tabpage'); // Product Tabs
    }
}