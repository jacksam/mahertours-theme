<?php
/**
 * The Header for our theme.
 *
 * @package progression
 * @since progression 1.0
 */
?><!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>  <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">

	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
 	 <link rel="icon" type="image/x-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico">
 	 <!-- <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"> -->
 	 <script src="https://use.fontawesome.com/f96ba5f76c.js"></script>
	
	<?php wp_head(); ?>

	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.prettyPhoto.js"></script>
 	 <link type="text/css" rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/prettyPhoto.css">
</head>

<body <?php body_class(); ?>>
<header>
	<div id="header-top">
		<div class="width-container">
			<div class="social-ico">
				<?php if (get_theme_mod( 'twitter_link_progression' )) : ?><a href="<?php echo get_theme_mod('twitter_link_progression'); ?>" target="_blank"><i class="fa fa-twitter"></i></a><?php endif; ?>
				<?php if (get_theme_mod( 'facebook_link_progression' )) : ?><a href="<?php echo get_theme_mod('facebook_link_progression'); ?>" target="_blank"><i class="fa fa-facebook"></i></a><?php endif; ?>
				<?php if (get_theme_mod( 'vimeo_link_progression' )) : ?><a href="<?php echo get_theme_mod('vimeo_link_progression'); ?>" target="_blank"><i class="fa fa-vimeo-square"></i></a><?php endif; ?>
				<?php if (get_theme_mod( 'youtube_link_progression' )) : ?><a href="<?php echo get_theme_mod('youtube_link_progression'); ?>" target="_blank"><i class="fa fa-youtube"></i></a><?php endif; ?>
				<?php if (get_theme_mod( 'google_link_progression' )) : ?><a href="<?php echo get_theme_mod('google_link_progression'); ?>" target="_blank"><i class="fa fa-google-plus"></i></a><?php endif; ?>
				<?php if (get_theme_mod( 'pinterest_link_progression' )) : ?><a href="<?php echo get_theme_mod('pinterest_link_progression'); ?>" target="_blank"><i class="fa fa-pinterest"></i></a><?php endif; ?>
				<?php if (get_theme_mod( 'dribbble_link_progression' )) : ?><a href="<?php echo get_theme_mod('dribbble_link_progression'); ?>" target="_blank"><i class="fa fa-dribbble"></i></a><?php endif; ?>
				<?php if (get_theme_mod( 'linkedin_link_progression' )) : ?><a href="<?php echo get_theme_mod('linkedin_link_progression'); ?>" target="_blank"><i class="fa fa-linkedin"></i></a><?php endif; ?>
				<?php if (get_theme_mod( 'flickr_link_progression' )) : ?><a href="<?php echo get_theme_mod('flickr_link_progression'); ?>" target="_blank"><i class="fa fa-flickr"></i></a><?php endif; ?>
				<?php if (get_theme_mod( 'instagram_link_progression' )) : ?><a href="<?php echo get_theme_mod('instagram_link_progression'); ?>" target="_blank"><i class="fa fa-instagram"></i></a><?php endif; ?>
				<?php if (get_theme_mod( 'tumblr_link_progression' )) : ?><a href="<?php echo get_theme_mod('tumblr_link_progression'); ?>" target="_blank"><i class="fa fa-tumblr"></i></a><?php endif; ?>
			</div>
			<div id="subscribe-right"><?php echo do_shortcode( stripslashes( get_theme_mod( 'header_subscribe_pro'))); ?><span id="top-divider">|</span><a href="/request-a-brochure">Request A Brochure</a></div>
      <div class="clearfix"></div>
		</div>
	</div><!-- close #header-top -->
	
	<div id="logo-container">
		<div class="width-container">
			<h1 id="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo get_theme_mod( 'logo_upload', get_template_directory_uri() . '/images/logo.png' ); ?>" alt="<?php bloginfo('name'); ?>" width="<?php echo get_theme_mod( 'logo_width', '165' ); ?>" /></a></h1>
			<nav>
				<?php if(function_exists('woocommerce_product_dropdown_categories')): ?> <?php global $woocommerce; ?>
					<!--div id="cart-icon-pro"><a href="<?php echo $woocommerce->cart->get_cart_url(); ?>"><i class="fa fa-shopping-cart"></i></a></div-->
				<?php endif; ?>
				<?php wp_nav_menu( array('theme_location' => 'primary', 'depth' => 4, 'menu_class' => 'sf-menu') ); ?>
			</nav>
		<div class="clearfix"></div>
		</div><!-- close .width-container -->
	</div><!-- close #logo-container -->
</header>

<?php /*?><?php get_template_part( 'header', 'search' ); ?><?php */?>