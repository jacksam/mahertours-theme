<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package progression
 * @since progression 1.0
 */
?>

<?php if ( ! dynamic_sidebar( 'Wide Footer Widget' ) ) : ?>
<?php endif; // end sidebar widget area ?>
<div class="clearfix"></div>
</div><!-- close #main -->

<div id="widget-area">
	<div class="width-container footer-<?php echo get_theme_mod('footer_cols', '4'); ?>-column">
		<?php if ( ! dynamic_sidebar( 'Footer Widgets' ) ) : ?>
		<?php endif; // end sidebar widget area ?>
	</div>
	<div class="clearfix"></div>
</div>
<footer>
	<div class="width-container">
		<div id="copyright">
			<?php echo get_theme_mod( 'copyright_textbox', '&copy; 2014 All Rights Reserved. Developed by ProgressionStudios' ); ?>
		<div class="clearfix"></div>
		</div><!-- close #copyright -->
	</div><!-- close .width-container -->
</footer>
<?php wp_footer(); ?>
</body>
</html>