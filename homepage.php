<?php
// Template Name: HomePage
/**
 *
 * @package progression
 * @since progression 1.0
 */

get_header(); ?>
	
<?php get_template_part( 'slider', 'progression' ); ?>

<div id="main">
	<div class="width-container">
		
		<!-- this code pull in the homepage content -->
		<?php while(have_posts()): the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>
		<div class="clearfix"></div>
	
		
	</div><!-- close .width-container -->
<?php get_footer(); ?>