<?php
/**
 * The template for displaying product category thumbnails within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product_cat.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = get_theme_mod('shop_col_progression', '3');

// Increase loop count
$woocommerce_loop['loop']++;
?>
<li class="product-category product column-<?php echo get_theme_mod('shop_col_progression', '3'); ?> <?php
	if ( $woocommerce_loop['loop'] % $woocommerce_loop['columns'] == 0 )
		echo 'last';
	elseif ( ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] == 0 )
		echo 'first';
	?>">
	<div class="product-container-pro">

	<?php do_action( 'woocommerce_before_subcategory', $category ); ?>

	<a href="<?php echo get_term_link( $category->slug, 'product_cat' ); ?>">
		
		<div class="product-image-pro">
		
		<?php
			/**
			 * woocommerce_before_subcategory_title hook
			 *
			 * @hooked woocommerce_subcategory_thumbnail - 10
			 */
			do_action( 'woocommerce_before_subcategory_title', $category );
		?>
		</div>

		<h3>
			<?php echo $category->name; ?>
		</h3>

		<?php if( $category->count > 0): ?>
			<?php
				if ( $category->count > 0 )
					echo apply_filters( 'woocommerce_subcategory_count_html', ' <div class="count-pro-cat">' . $category->count . '', $category );
			?>
			<?php _e( 'Trips', 'progression' ); ?></div>
		<?php endif; ?>

		<?php
			/**
			 * woocommerce_after_subcategory_title hook
			 */
			do_action( 'woocommerce_after_subcategory_title', $category );
		?>

	</a>

	<?php do_action( 'woocommerce_after_subcategory', $category ); ?>
	</div><!-- close .product-container-pro -->
</li>
