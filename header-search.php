<?php if(is_page_template('homepage.php') || is_page_template('blog-slider.php')): ?>
	
	<?php if(function_exists('woocommerce_product_dropdown_categories')): ?>
	<div id="search-container-pro">
		<div id="panel-search">
			<div class="width-container">
				<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
					<label class="screen-reader-text" for="s"><?php _e( 'Search for:', 'progression' ); ?></label>
					<input type="text" class="search-field" value="<?php echo get_search_query(); ?>" name="s" id="s" placeholder="<?php _e( 'Search all travel destinations', 'progression' ); ?>" />
						<div class="submit-button-pro">
							<span><?php _e( 'Explore', 'progression' ); ?></span>
							<input type="submit" class="search-submit" value="" />
						</div>
					<input type="hidden" name="post_type" value="product" />
				</form>
			</div><!-- close .width-container -->
		</div>
	</div>
	<?php else: ?>
	<div id="search-container-pro">
		<div id="panel-search">
			<div class="width-container">
				<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
					<label>
						<span class="screen-reader-text"><?php _ex( 'Search for:', 'label', 'progression' ); ?></span>
						<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Start Searching...', 'progression' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s">
					</label>
					<div class="submit-button-pro">
						<span><?php _e( 'Explore', 'progression' ); ?></span>
					<input type="submit" class="search-submit" value="">
				</div>
				</form>
			</div><!-- close .width-container -->
		</div>
	</div>
	<?php endif; ?>
	
	
<?php endif; ?>