<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package progression
 */
?>
<div id="content-container">
<?php if ( has_post_thumbnail() ) { ?>
		<div class="featured-image">
		<?php
		 	// check if the post has a Post Thumbnail assigned to it.
        	the_post_thumbnail('featured-image-scale-crop');
        ?>	
      	</div>
      	<?php } ?>
	<?php the_content(); ?>
	<?php
		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'progression' ),
			'after'  => '</div>',
		) );
	?>
	<?php /*?><?php edit_post_link( __( 'Edit', 'progression' ), '<p class="edit-link">', '</p>' ); ?><?php */?>
  <div class="page-actions clearfix">
    <div id="font-sizer"><?php if(function_exists('fontResizer_place')) { fontResizer_place(); }?><span>Change the text size</span></div>
    <div id="print-page"><?php if(function_exists('pf_show_link')){echo pf_show_link();} ?></div>
  </div>
	<?php
		// If comments are open or we have at least one comment, load up the comment template
		if ( comments_open() || '0' != get_comments_number() )
			comments_template();
	?>
</div>